//
//  Categories.swift
//  Recipository
//
//  Created by Jakob Hviid on 22/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import Foundation
import CoreData


class Categories: NSManagedObject {

    class CoreDataKeys {
        static let EntityName = "Category"
        static let ObjectId = "objectId"
        static let Name = "name"
    }
    
    class JsonKeys {
        static let Array = "Categories"
        static let Id = "Id"
        static let Name = "Name"
    }
}
