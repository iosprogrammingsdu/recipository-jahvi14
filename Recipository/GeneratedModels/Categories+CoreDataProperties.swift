//
//  Categories+CoreDataProperties.swift
//  Recipository
//
//  Created by Jakob Hviid on 22/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Categories {

    @NSManaged var objectId: NSNumber?
    @NSManaged var name: String?
    @NSManaged var recipes: NSSet?

}
