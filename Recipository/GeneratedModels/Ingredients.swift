//
//  Ingredients.swift
//  Recipository
//
//  Created by Jakob Hviid on 22/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import Foundation
import CoreData


class Ingredients: NSManagedObject {

    class CoreDataKeys {
        static let EntityName = "Ingredient"
        static let ObjectId = "objectId"
        static let RecipeId = "recipeId"
        static let Ingredient = "ingredient"
        static let Amount = "amount"
    }
    
    class JsonKeys {
        static let Array = "Ingredients"
        static let Id = "Id"
        static let RecipeId = "RecipeId"
        static let Ingredient = "Ingredient"
        static let Amount = "Amount"
    }
}
