//
//  Recipes.swift
//  Recipository
//
//  Created by Jakob Hviid on 22/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import Foundation
import CoreData
import Alamofire


class Recipes: NSManagedObject {

    class CoreDataKeys {
        static let EntityName = "Recipe"
        static let ObjectId = "objectId"
        static let CategoryId = "categoryId"
        static let Name = "name"
        static let Hearts = "hearts"
        static let Difficulty = "difficulty"
        static let Directions = "directions"
        static let Image = "image"
        static let ImageUrl = "imageUrl"
    }
    
    class JsonKeys {
        static let Array = "Recipes"
        static let Id = "Id"
        static let CategoryId = "CategoryId"
        static let Name = "Name"
        static let Hearts = "Hearts"
        static let Difficulty = "Difficulty"
        static let Directions = "Directions"
        static let PictureBase64 = "PictureBase64"
        static let PicturePath = "PicturePath"
    }
}
