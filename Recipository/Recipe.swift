//
//  Recipe.swift
//  Recipository
//
//  Created by Jakob Hviid on 21/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import Foundation

class Recipe : NSObject {
    var Id = 0
    var CategoryId = 0
    var Name = ""
    var Image = ""
    var Difficulty = 0
    var Hearts = 0
    var Directions = ""
    var Ingredients = [Ingredient]()
    
    init (id: Int, categoryId: Int, name:String, hearts: Int, difficulty: Int, directions: String, image:String, ingredients: [Ingredient]) {
        self.Id = id
        self.CategoryId = categoryId
        self.Name = name
        self.Image = image
        self.Difficulty = difficulty
        self.Hearts = hearts
        self.Directions = directions
        self.Ingredients = ingredients
    }
    
    class CoreDataKeys {
        static let EntityName = "Recipe"
        static let Id = "id"
        static let CategoryId = "categoryId"
        static let Name = "name"
        static let Hearts = "hearts"
        static let Difficulty = "difficulty"
        static let Directions = "directions"
        static let Image = "image"
    }
    
    class JsonKeys {
        static let Array = "Recipes"
        static let Id = "Id"
        static let CategoryId = "CategoryId"
        static let Name = "Name"
        static let Hearts = "Hearts"
        static let Difficulty = "Difficulty"
        static let Directions = "Directions"
        static let PictureBase64 = "PictureBase64"
    }
}
