//
//  RecipesViewController.swift
//  Recipository
//
//  Created by Jakob Hviid on 11/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import UIKit

var globalRecipesViewController: RecipesViewController!

class RecipesViewController: UITableViewController /* , UIViewControllerPreviewingDelegate*/ {
    
    @IBOutlet weak var titleBar: UINavigationItem!
    
    @IBOutlet var tableViewOutlet: UITableView!
    
    @IBOutlet weak var addButtonOutlet: UIBarButtonItem!
    
    var recipes: [Recipes]!
    
    var passToDetailsView: Recipes!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //Til når jeg har tid til at sætte 3d touch op: http://www.the-nerd.be/2015/10/06/3d-touch-peek-and-pop-tutorial/
        /*
        if( traitCollection.forceTouchCapability == .Available){
            registerForPreviewingWithDelegate(self, sourceView: view)
        }
        */
        
        print("Saved Global Varialbe for RecipesViewController")
        globalRecipesViewController = self
        
    }
        
    func reloadFromAddRecipeModal(name: String, picture: UIImage, diff: Int, hearts: Int, ingredients: [Dictionary<String,String>], instructions:String) {
    
        addButtonOutlet.enabled = false
        
        //enable cell interaction
        for cell in tableViewOutlet.visibleCells {
            cell.userInteractionEnabled = false
            cell.textLabel!.textColor = UIColor.grayColor()
        }
        
        // Sends data for creation!
        DataManager.RecipeHandler.addRecipe(
            globalCurrentCategoryId as Int,
            name: name,
            picture: picture,
            diff: diff,
            hearts: hearts,
            ingredients: ingredients,
            instructions: instructions) { () -> Void in
                
                self.recipes = DataManager.RecipeHandler.getRecipes(globalCurrentCategoryId)
                
                self.tableViewOutlet.reloadData()
                //disable cell interaction
                for cell in self.tableViewOutlet.visibleCells {
                    cell.userInteractionEnabled = true
                    cell.textLabel!.textColor = UIColor.blackColor()
                }
                self.addButtonOutlet.enabled = true
                CATransaction.flush()
                
                print("Save Action Complete")
        }
    }
    
    func reloadCoreData() {
        addButtonOutlet.enabled = false
        
        //enable cell interaction
        for cell in tableViewOutlet.visibleCells {
            cell.userInteractionEnabled = false
            cell.textLabel!.textColor = UIColor.grayColor()
        }
        
        DataManager.updateCoreDataFromServer { () -> Void in
            self.recipes = DataManager.RecipeHandler.getRecipes(globalCurrentCategoryId)
            
            self.tableViewOutlet.reloadData()
            //disable cell interaction
            for cell in self.tableViewOutlet.visibleCells {
                cell.userInteractionEnabled = true
                cell.textLabel!.textColor = UIColor.blackColor()
            }
            self.addButtonOutlet.enabled = true
            CATransaction.flush()
            
            print("reload Complete")

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        cell.textLabel?.text = recipes![indexPath.row].name

        return cell
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        passToDetailsView = recipes![indexPath.row]
        
        return indexPath
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            let alert = UIAlertController(title: "Delete Recipe",
                message: "Are you sure you want to delete this recipe?",
                preferredStyle: .Alert)
            
            let deleteAction = UIAlertAction(title: "Delete",
                style: .Default,
                handler: { (action:UIAlertAction) -> Void in
                    let deleteThis = self.recipes[indexPath.row].objectId! as NSNumber
                    DataManager.RecipeHandler.deleteRecipe(deleteThis, completionHandler: self.reloadCoreData)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel",
                style: .Default) { (action: UIAlertAction) -> Void in
            }
            
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            
            presentViewController(alert,
                animated: true,
                completion: nil)
        }
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    /* Segue Transition */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showDetails" {
            let navController = segue.destinationViewController as! UINavigationController
            
            let svc = navController.viewControllers.first as! RecipeController;
                
            svc.recipe = passToDetailsView
            
            svc.navigationHeader.title = passToDetailsView.name
            
        }
    }
    
}
