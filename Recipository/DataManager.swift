//
//  Globals.swift
//  Recipository
//
//  Created by Jakob Hviid on 11/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

var recipositoryData = [Category]()
var tempData = [Category]()

// Guide for later: http://www.raywenderlich.com/82706/working-with-json-in-swift-tutorial
class DataManager {
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        let session = NSURLSession.sharedSession()
        
        // Use NSURLSession to get data from an NSURL
        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    let statusError = NSError(domain:"dk.recipository", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                }
            }
        })
        
        loadDataTask.resume()
    }
    
    class func getContext() ->  NSManagedObjectContext{
        let context: NSManagedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        return context
    }
    
    private class func emptyCoreData() {
        let context = getContext()
        
        let ingredientsRequest = NSFetchRequest(entityName: Ingredients.CoreDataKeys.EntityName)
        let recipesRequest = NSFetchRequest(entityName: Recipes.CoreDataKeys.EntityName)
        let categoriesRequest = NSFetchRequest(entityName: Categories.CoreDataKeys.EntityName)
        
        do {
            let ingredients = try context.executeFetchRequest(ingredientsRequest)
            
            for item in ingredients as! [NSManagedObject] {
                context.deleteObject(item)
            }
        } catch {
            print("datamanager.emprycoredata failed deleting core data ingredients")
        }
        
        do {
            let recipes = try context.executeFetchRequest(recipesRequest)
            
            for item in recipes as! [NSManagedObject] {
                context.deleteObject(item)
            }
        } catch {
            print("datamanager.emprycoredata failed deleting core data recipes")
        }
        
        do {
            let categories = try context.executeFetchRequest(categoriesRequest)
            
            for item in categories as! [NSManagedObject] {
                context.deleteObject(item)
            }
        } catch {
            print("datamanager.emprycoredata failed at deleting categories")
        }
        
        do {
        
            try context.save()
        } catch {
            print("datamanager.emprycoredata failed deleting core data contents")
        }
    }
    
    class func updateCoreDataFromServer(completionHandler: () -> Void ) {
        let context = getContext()
        
        emptyCoreData()
        
        loadDataFromURL(NSURL(string:"http://recipository.dk/api/categories/")!) { (data, error) -> Void in
            //tjekker for den har hentet noget.
            if let json = data {
                
                let categories = JSON(data: json).array //categories er json i deres verden.
                
                //first go through categories
                for category in categories! {
                    
                    //Create category for later addition to tempData
                    let newCategory = NSEntityDescription.insertNewObjectForEntityForName(Categories.CoreDataKeys.EntityName, inManagedObjectContext: context) as! Categories
                    newCategory.objectId = category[Categories.JsonKeys.Id].int!
                    newCategory.name = category[Categories.JsonKeys.Name].string!
                    
                    do {
                        try context.save()
                    } catch {
                        print("datamanager.updatecoredatafromserver could not save category: \(category[Categories.JsonKeys.Name].string!)")
                    }
                    
                    //now go through recipes
                    let extractedRecipes = category[Recipes.JsonKeys.Array].array
                    let recipeRelation = newCategory.mutableSetValueForKey("recipes")
                    
                    for recipe in extractedRecipes! {
                        
                        let newRecipe = NSEntityDescription.insertNewObjectForEntityForName(Recipes.CoreDataKeys.EntityName, inManagedObjectContext: context) as! Recipes
                        newRecipe.objectId = recipe[Recipes.JsonKeys.Id].int!
                        newRecipe.categoryId = recipe[Recipes.JsonKeys.CategoryId].int!
                        newRecipe.name = recipe[Recipes.JsonKeys.Name].string!
                        newRecipe.hearts = recipe[Recipes.JsonKeys.Hearts].number!
                        newRecipe.difficulty = recipe[Recipes.JsonKeys.Difficulty].number!
                        newRecipe.directions = recipe[Recipes.JsonKeys.Directions].string!
                        newRecipe.image = recipe[Recipes.JsonKeys.PictureBase64].string!
                        newRecipe.imageUrl = recipe[Recipes.JsonKeys.PicturePath].string!
                        
                        recipeRelation.addObject(newRecipe)
                        
                        do {
                            try newRecipe.managedObjectContext?.save()
                            //try context.save()
                        } catch {
                            print("datamanager.updatecoredatafromserver could not save recipe: \(recipe[Recipes.JsonKeys.Name].string!)")
                        }
                        
                        //now go through ingredients
                        let extractedIngredients = recipe[Ingredients.JsonKeys.Array].array
                        let ingredientRelation = newRecipe.mutableSetValueForKey("ingredients")
                        for ingredient in extractedIngredients! {
                            
                            let newIngredient = NSEntityDescription.insertNewObjectForEntityForName(Ingredients.CoreDataKeys.EntityName, inManagedObjectContext: context) as! Ingredients
                            newIngredient.objectId = ingredient[Ingredients.JsonKeys.Id].int!
                            newIngredient.recipeId = ingredient[Ingredients.JsonKeys.RecipeId].int!
                            newIngredient.ingredient = ingredient[Ingredients.JsonKeys.Ingredient].string!
                            newIngredient.amount = ingredient[Ingredients.JsonKeys.Amount].string!
                            
                            ingredientRelation.addObject(newIngredient)
                            
                            do {
                                try newIngredient.managedObjectContext?.save()
                                //try context.save()
                            } catch {
                                print("datamanager.updatecoredatafromserver could not save recipe: \(ingredient[Ingredients.JsonKeys.Ingredient].string!)")
                            }
                        }
                    }
                }
            }
            
            print("Reloading data from server completed")
            
            //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
            dispatch_async(dispatch_get_main_queue(), {
                completionHandler()
            })
            
        }
    }
    
    
    
    class CategoryHandler {
        class func getCategories() -> [Categories] {
            let context = DataManager.getContext()
            
            //reading from
            let request = NSFetchRequest(entityName: Categories.CoreDataKeys.EntityName)
            request.returnsObjectsAsFaults = false
            
            var returnThis = [Categories]()
            
            do {
                let results = try context.executeFetchRequest(request)
                
                //går igennem resultaterne
                if results.count > 0 {
                    returnThis = results as! [Categories]
                }
                
            } catch {
                print("error - could not load categories")
            }
            
            return returnThis
        }
        
        /* Trows error: CoreData: error: Failed to call designated initializer on NSManagedObject class
        class func getCategory(categoryId: NSNumber) -> Categories {
            let context = DataManager.getContext()
            
            //reading from
            let request = NSFetchRequest(entityName: Categories.CoreDataKeys.EntityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "\(Categories.CoreDataKeys.ObjectId) == %@", categoryId as NSNumber)
            
            var returnThis: Categories = Categories()
            
            do {
                let results = try context.executeFetchRequest(request)
                
                //går igennem resultaterne
                if results.count > 0 {
                    let result = results[0] as! Categories
                    
                    returnThis = result
                }
                
            } catch {
                print("error - could not load recipes")
            }
            
            return returnThis
        }
        */
        
        class func addCategory(categoryName: String?, completionHandler:() -> Void )
        {
            if let cat = categoryName {
                let jsonObject = [
                    "Name" : "\(cat)"
                ]
                
                Alamofire.request(.POST, "http://recipository.dk/api/categories", parameters: jsonObject)
                    .responseJSON { response in
                        //print(response.request)  // original URL request
                        //print(response.response) // URL response
                        //print(response.data)     // server data
                        //print(response.result)   // result of response serialization
                        
                        if let data = response.result.value {
                            let jsonResponse = JSON(data)
                            
                            print("The New Category Added Got Category Id: " + String(jsonResponse["Id"].int))
                            
                            
                            if let id = jsonResponse[Categories.JsonKeys.Id].int {
                                let context = DataManager.getContext()
                                //Create category for later addition to tempData
                                let newCategory = NSEntityDescription.insertNewObjectForEntityForName(Categories.CoreDataKeys.EntityName, inManagedObjectContext: context) as! Categories
                                newCategory.objectId = id
                                newCategory.name = cat
                                
                                do {
                                    try context.save()
                                    
                                    //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                    dispatch_async(dispatch_get_main_queue(), {
                                        completionHandler()
                                    })
                                    
                                } catch {
                                    print("failed saving new category to core data - reloading data from server")
                                    
                                    //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                    dispatch_async(dispatch_get_main_queue(), {
                                        DataManager.updateCoreDataFromServer(completionHandler)
                                    })
                                }
                                
                            } else {
                                print("faild saving new category to server - reloading data from server.")
                                
                                //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                dispatch_async(dispatch_get_main_queue(), {
                                    DataManager.updateCoreDataFromServer(completionHandler)
                                })
                            }
                        }
                }
            }
        }
        
        class func deleteCategory(categoryId: NSNumber?, completionHandler:() -> Void )
        {
            if let cat = categoryId {
                let jsonObject = [
                    "Id" : "\(cat)"
                ]
                
                Alamofire.request(.DELETE, "http://recipository.dk/api/categories", parameters: jsonObject)
                    .responseJSON { response in
                        //print(response.request)  // original URL request
                        //print(response.response) // URL response
                        //print(response.data)     // server data
                        //print(response.result)   // result of response serialization
                        
                        if let data = response.result.value {
                            let jsonResponse = JSON(data)
                            
                            
                            print("The Category Deleted Got Category Id: " + String(jsonResponse["Id"].int))
                            
                            //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                            dispatch_async(dispatch_get_main_queue(), {
                                DataManager.updateCoreDataFromServer(completionHandler)
                            })

                            
                            // TODO: getCategory fejler i core data, ergo vi reloader bare fra serveren i stedet selv om det er langsomere.
                            
                            /*
                            
                            if let id = jsonResponse[Categories.JsonKeys.Id].int {
                                let context = DataManager.getContext()
                                //Find category and delete
                                let categoryToDelete: Categories = DataManager.CategoryHandler.getCategory(id as NSNumber)
                                context.deleteObject(categoryToDelete)
                                
                                do {
                                    try context.save()
                                    
                                    //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                    dispatch_async(dispatch_get_main_queue(), {
                                        completionHandler()
                                    })
                                    
                                } catch {
                                    print("failed deleting category from core data - reloading data from server")
                                    
                                    //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                    dispatch_async(dispatch_get_main_queue(), {
                                        DataManager.updateCoreDataFromServer(completionHandler)
                                    })
                                }

                                
                            } else {
                                print("faild deleting category from server - reloading data from server.")
                                
                                //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                dispatch_async(dispatch_get_main_queue(), {
                                    DataManager.updateCoreDataFromServer(completionHandler)
                                })
                            }
                            */
                        }
                }
            }
        }
    }
    
    class RecipeHandler {
        
        
        class func getRecipes() -> [Recipes] {
            let context = DataManager.getContext()
            
            //reading from
            let request = NSFetchRequest(entityName: Recipes.CoreDataKeys.EntityName)
            request.returnsObjectsAsFaults = false
            //request.predicate = NSPredicate(format: (Recipe.CoreDataKeys.CategoryId + " == %@"), categoryId)
            
            var returnThis = [Recipes]()
            
            do {
                let results = try context.executeFetchRequest(request)
                
                //går igennem resultaterne
                if results.count > 0 {
                    returnThis = results as! [Recipes]
                }
                
            } catch {
                print("error - could not load recipes")
            }
            
            return returnThis
        }
        
        class func getRecipes(categoryId: NSNumber) -> [Recipes] {
            //return getRecipes().filter({$0.CategoryId == categoryId})
            
            let context = DataManager.getContext()
            
            //reading from
            let request = NSFetchRequest(entityName: Recipes.CoreDataKeys.EntityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "\(Recipes.CoreDataKeys.CategoryId) == %@", categoryId as NSNumber)
            
            var returnThis = [Recipes]()
            
            do {
                let results = try context.executeFetchRequest(request)
                
                //går igennem resultaterne
                if results.count > 0 {
                    returnThis = results as! [Recipes]
                }
                
            } catch {
                print("error - could not load recipes")
            }
            
            return returnThis
            
        }
        
        class func getRecipe (recipeId: NSNumber) -> Recipes {
            
            let context = DataManager.getContext()
            
            //reading from
            let request = NSFetchRequest(entityName: Recipes.CoreDataKeys.EntityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "\(Recipes.CoreDataKeys.CategoryId) == %@", recipeId as NSNumber)
            
            var returnThis: Recipes = Recipes()
            
            do {
                let results = try context.executeFetchRequest(request)
                
                //går igennem resultaterne
                if results.count > 0 {
                    let result = (results[0] as! Recipes)
                    
                    returnThis = result
                }
                
            } catch {
                print("error - could not load recipes")
            }
            
            
            
            return returnThis
        }
        
        class func addRecipe (categoryId: Int, name: String, picture: UIImage, diff: Int, hearts: Int, ingredients:[Dictionary<String,String>],instructions:String, completionHandler: () -> Void) {
            
            let imageJsonObject: Dictionary = [
                "ImagePost" : "Please Return Image Data",
            ]
            
            let imageData = UIImagePNGRepresentation(picture)
            
            // CREATE AND SEND REQUEST ----------
            
            let imageUrlRequest = AlamofireExtentions.urlRequestWithComponents("http://recipository.dk/imageupload/PostImage", parameters: imageJsonObject, imageData: imageData!)
            
            Alamofire.upload(imageUrlRequest.0, data: imageUrlRequest.1)
                .progress { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                    print(totalBytesWritten)
                    dispatch_async(dispatch_get_main_queue()) {
                        print("Total bytes written on main queue: \(totalBytesWritten) Expected: \(totalBytesExpectedToWrite)")
                    }
                }
                .responseJSON { responseFromImagePost in
                    if let imageDataValidated = responseFromImagePost.result.value {
                        
                        let imageData = JSON(imageDataValidated)
                        
                        //now we got the image links - time to build the recipe creation request
                        
                        let jsonObject: Dictionary = [
                            "CategoryId" : "\(categoryId)",
                            "Name" : "\(name)",
                            "Difficulty" : "\(diff)",
                            "Hearts" : "\(hearts)",
                            "Directions" : "\(instructions)",
                            "PictureBase64" : "\(imageData["Base64"].string!)",
                            "PicturePath" : "\(imageData["Path"].string!)",
                        ]
                        
                        Alamofire.request(.POST, "http://recipository.dk/api/recipes", parameters: jsonObject)
                            .responseJSON { response in
                                
                                if let data = response.result.value {
                                    let jsonResponse = JSON(data)
                                    
                                    print("The New Recipe Added And Got Recipe Id: " + String(jsonResponse["Id"].int))
                                    
                                    //print(response.request!.description)  // original URL request
                                    //print(response.response) // URL response
                                    //print(JSON(response.data!))     // server data
                                    //print(response.result)   // result of response serialization
                                    
                                    let recipeid = jsonResponse["Id"].int
                                    
                                    if let rid = recipeid {
                                        for i in ingredients {
                                            
                                            let ingredientJsonObject: Dictionary = [
                                                "RecipeId" : "\(rid)",
                                                "Amount" : "\(i[Ingredients.CoreDataKeys.Amount]!)",
                                                "Ingredient" : "\(i[Ingredients.CoreDataKeys.Ingredient]!)",
                                            ]
                                            
                                            Alamofire.request(.POST, "http://recipository.dk/api/ingredients/", parameters: ingredientJsonObject)
                                                .responseJSON { response in
                                                    
                                                    if let data = response.result.value {
                                                        let ingredientJsonObjectResponse = JSON(data)
                                                        
                                                        print("The New Ingredient Added And Got Ingredient Id: " + String(ingredientJsonObjectResponse["Id"].int))                                    }
                                            }
                                        }
                                    } else {
                                        print("Failed to save recipe")
                                    }
                                    
                                    //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                                    dispatch_async(dispatch_get_main_queue(), {
                                        DataManager.updateCoreDataFromServer(completionHandler)
                                    })
                                    
                                }
                        }
                    } else {
                        print("Image Too Large for the Server API - JSON Malformed")
                    }
                    
            }
        }

            
        class func deleteRecipe(recipeId: NSNumber?, completionHandler:() -> Void )
        {
            if let rec = recipeId {
                let jsonObject = [
                    "Id" : "\(rec)"
                ]
                
                Alamofire.request(.DELETE, "http://recipository.dk/api/recipes", parameters: jsonObject)
                    .responseJSON { response in
                        //print(response.request)  // original URL request
                        //print(response.response) // URL response
                        //print(response.data)     // server data
                        //print(response.result)   // result of response serialization
                        
                        if let data = response.result.value {
                            let jsonResponse = JSON(data)
                            
                            
                            print("The Recipe was Deleted had Id: " + String(jsonResponse["Id"].int))
                            
                            //Den her linje er nu nødvendig når man opdaterer UI fra bagrunden.
                            dispatch_async(dispatch_get_main_queue(), {
                                DataManager.updateCoreDataFromServer(completionHandler)
                            })
                        }
                }
            }
        }
        
    }
    
    class IngredientsHandler {
        class func getIngredients(recipeId: NSNumber) -> [Ingredients] {
            let context = DataManager.getContext()
            
            //reading from
            let request = NSFetchRequest(entityName: Ingredients.CoreDataKeys.EntityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "\(Ingredients.CoreDataKeys.RecipeId) = %@", recipeId as NSNumber)
            
            var returnThis = [Ingredients]()
            
            do {
                let results = try context.executeFetchRequest(request)
                
                returnThis = results as! [Ingredients]
                
            } catch {
                print("error - could not load categories")
            }
            
            return returnThis

        }
    }
}