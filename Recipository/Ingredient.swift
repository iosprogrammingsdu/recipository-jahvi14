//
//  Ingredient.swift
//  Recipository
//
//  Created by Jakob Hviid on 21/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import Foundation

class Ingredient : NSObject {
    var Id = 0
    var Ingredient = ""
    var Amount = ""
    var RecipeId = 0
    
    init(id: Int, ingredient: String, amount: String, recipeId: Int) {
        self.Id = id
        self.Ingredient = ingredient
        self.Amount = amount
        self.RecipeId = recipeId
    }
    
    class CoreDataKeys {
        static let EntityName = "Ingredient"
        static let Id = "id"
        static let RecipeId = "recipeId"
        static let Ingredient = "ingredient"
        static let Amount = "amount"
    }
    
    class JsonKeys {
        static let Array = "Ingredients"
        static let Id = "Id"
        static let RecipeId = "RecipeId"
        static let Ingredient = "Ingredient"
        static let Amount = "Amount"
    }
}