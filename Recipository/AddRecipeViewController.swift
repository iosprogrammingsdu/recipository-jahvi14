//
//  AddRecipeViewController.swift
//  Recipository
//
//  Created by Jakob Hviid on 26/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import UIKit

class AddRecipeViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let imagePicker = UIImagePickerController()
    
    var imageToPass: UIImage!
    
    @IBOutlet weak var RecipeNameOutlet: UITextView!
    
    @IBOutlet weak var ImageViewOutlet: UIImageView!
    
    @IBOutlet weak var DiffLabelOutlet: UILabel!
    @IBOutlet weak var DiffStepperOutlet: UIStepper!
    
    @IBOutlet weak var HeartsLabelOutlet: UILabel!
    @IBOutlet weak var HeartsStepperOutlet: UIStepper!
    
    @IBOutlet weak var IngredientsAmountOutlet: UITextView!
    @IBOutlet weak var IngredientsOutlet: UITextView!
    
    @IBOutlet weak var InstructionsOutlet: UITextView!
    
    @IBAction func cancelToRecipeController() {
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    @IBAction func saveRecipe() {
        print("Save Action running")
        
        //fixing ingredients array for creation
        var ingredientsArray: [Dictionary<String,String>] = [Dictionary<String,String>]()
       
        var ingredient: Dictionary<String,String> = Dictionary<String,String>()
        ingredient[Ingredients.CoreDataKeys.Amount] = IngredientsAmountOutlet.text!
        ingredient[Ingredients.CoreDataKeys.Ingredient] = IngredientsOutlet.text!
        
        ingredientsArray.append(ingredient)
        
        
        globalRecipesViewController.reloadFromAddRecipeModal(
            RecipeNameOutlet.text!,
            picture: imageToPass.resizeToWidth(720),
            diff: Int(DiffLabelOutlet.text!)!,
            hearts: Int(HeartsLabelOutlet.text!)!,
            ingredients: ingredientsArray,
            instructions: InstructionsOutlet.text!)

        
        //dismisses the modal
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        imagePicker.delegate = self
        
        DiffStepperOutlet.wraps = true
        HeartsStepperOutlet.wraps = true
        DiffStepperOutlet.autorepeat = true
        HeartsStepperOutlet.autorepeat = true
        DiffStepperOutlet.maximumValue = 5
        HeartsStepperOutlet.maximumValue = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Stepper actions
    
    @IBAction func DiffValueChanged(sender: UIStepper) {
        DiffLabelOutlet.text = Int(sender.value).description
    }
    
    @IBAction func HeartsValueChanged(sender: UIStepper) {
        HeartsLabelOutlet.text = Int(sender.value).description
    }

    // MARK: - UIImagePickerController related
    
    @IBAction func ChoosePictureAction(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func TakePictureAction(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .Camera
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //ImageViewOutlet.contentMode = .ScaleAspectFit
            ImageViewOutlet.image = pickedImage
            imageToPass = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
