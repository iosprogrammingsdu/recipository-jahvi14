//
//  RecipeController.swift
//  Recipository
//
//  Created by Jakob Hviid on 11/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import UIKit
import MessageUI

class RecipeController: UIViewController, UIScrollViewDelegate, MFMailComposeViewControllerDelegate {
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var navigationHeader: UINavigationItem!
    
    @IBOutlet weak var printButton: UIBarButtonItem!
    @IBOutlet weak var emailButton: UIBarButtonItem!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var difficualtyLabel: UILabel!
    @IBOutlet weak var heartsLabel: UILabel!
    @IBOutlet weak var ingredientsTextView: UITextView!
    @IBOutlet weak var descriptionTextView: UITextView!

    var recipe : Recipes!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let model = recipe {
            
            mainImage.image = ImageHandler.DecodeFromBase64(model.image!)
            
            titleLabel.text = model.name!
            
            difficualtyLabel.text = "Difficulty: " + String(model.difficulty!)
            
            heartsLabel.text = "Hearts: " + String(model.hearts!)
            
            ingredientsTextView.text = ""
            
            for i in model.ingredients! {
                let ingredient = i as! Ingredients
                
                ingredientsTextView.text = ingredientsTextView.text + ingredient.amount! + " " + ingredient.ingredient! + "\r\n"
            }
            
            descriptionTextView.text = model.directions
        
        } else {
            
            for subview in scrollView.subviews {
                subview.hidden = true
            }
            printButton.enabled = false
            emailButton.enabled = false
            navigationHeader.title = ""
            
            let label = UILabel(frame: CGRectMake(0, 0, 300, 80))
            label.center = self.view.center
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.Center
            label.text = "Please choose a recipe in the \n category menu to the right."
            self.view.addSubview(label)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - MFMailComposeView
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func emailRecipe(sender: AnyObject) {
        //Check to see the device can send email.
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            var ingredients = ""
            
            for i in recipe.ingredients! {
                let ingredient = i as! Ingredients
                
                ingredients = ingredients + "<p>\(ingredient.amount!) - \(ingredient.ingredient!)</p>\n"
                
            }
            
            //Set the subject and message of the email
            mailComposer.setSubject(recipe.name!)
            
            mailComposer.setMessageBody(
                "<html>" +
                    "<body>" +
                    "<h2>\(recipe.name!)</h2>" +
                    "<img src='http://recipository.dk/\(recipe.imageUrl!)' style='max-height: 400px; max-width: 100%;'>" +
                    ingredients +
                    //"<p>Difficulty: \(recipe.difficulty!)</p>" +
                    //"<p>Hearts: \(recipe.difficulty!)</p>" +
                    "<p>\(recipe.directions!)</p>" +
                    "</body>" +
                "</html>", isHTML: true)
            
            /*
            // Til hvis jeg senere vil bruge attachments i stedet for html.
            if let fileData = NSData(base64EncodedString: recipe.image!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters) {
                print("File data loaded.")
                mailComposer.addAttachmentData(fileData, mimeType: "image/png", fileName: "RecipePicture")
            }
            */
            
            self.presentViewController(mailComposer, animated: true, completion: nil)
        }
    }

    // MARK: Print functionality
    
    @IBAction func printRecipe(sender: UIButton) {
        let printController = UIPrintInteractionController.sharedPrintController()
        
        let printInfo = UIPrintInfo(dictionary:nil)
        printInfo.outputType = UIPrintInfoOutputType.General
        printInfo.jobName = "print Job"
        printController.printInfo = printInfo
        
        
        // MARK: Prepareing Data for Print
        
        var ingredients = ""
        
        for i in recipe.ingredients! {
            let ingredient = i as! Ingredients
            
            ingredients = ingredients + "<p>\(ingredient.amount!) - \(ingredient.ingredient!)</p>\n"
            
        }
        
        let html: String = "<html>" +
            "<body>" +
            "<h2>\(recipe.name!)</h2>" +
            "<img src='http://recipository.dk/\(recipe.imageUrl!)' style='max-height: 300px'>" +
            ingredients +
            "<p>\(recipe.directions!)</p>" +
            "</body>" +
        "</html>"
        
        // MARK: Finishing up print job.
        
        let formatter = UIMarkupTextPrintFormatter(markupText: html)
        formatter.contentInsets = UIEdgeInsets(top: 35, left: 35, bottom: 35, right: 35)
        printController.printFormatter = formatter
        
        printController.presentAnimated(true, completionHandler: nil)
    }

    
    // MARK: Modal interaction
    @IBAction func cancelToRecipeController(segue:UIStoryboardSegue) {
        print("calcel to recipe controller ran")
    }
    
    
    // MARK: - UIScrollView
    
    /*
    override func viewDidLayoutSubviews() {
        let scrollViewBounds = scrollView.bounds
        let containerViewBounds = contentView.bounds
        
        var scrollViewInsets = UIEdgeInsetsZero
        scrollViewInsets.top = scrollViewBounds.size.height/2.0;
        scrollViewInsets.top -= contentView.bounds.size.height/2.0;
        
        scrollViewInsets.bottom = scrollViewBounds.size.height/2.0
        scrollViewInsets.bottom -= contentView.bounds.size.height/2.0;
        scrollViewInsets.bottom += 1
        
        scrollView.contentInset = scrollViewInsets
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
