//
//  ImageHandler.swift
//  Recipository
//
//  Created by Jakob Hviid on 15/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import UIKit
import Alamofire

class ImageHandler {
    class func DecodeFromBase64(encodedImageData:String) -> UIImage {
        return UIImage(data: NSData(base64EncodedString: encodedImageData, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!)!
    }
    
    class func EncodeToBase64(imageToEncode:UIImage) -> String {
        let imageData = UIImagePNGRepresentation(imageToEncode)!
        return imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
    }
    
    
}

extension UIImage{
    func toBase64() -> String{
        return ImageHandler.EncodeToBase64(self)
    }
}

extension String{
    func toUIImage() -> UIImage{
        return ImageHandler.DecodeFromBase64(self)
    }
    
    
}