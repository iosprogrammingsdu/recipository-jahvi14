//
//  CategoriesViewController.swift
//  Recipository
//
//  Created by Jakob Hviid on 11/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import UIKit

var globalCurrentCategoryId: NSNumber = 0

class CategoriesViewController: UITableViewController {

    var categories: [Categories] = [Categories]()
    var passToRecipesView: [Recipes] = [Recipes]()
    var titleToPass: String = "Category Name"
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBOutlet var tableOutlet: UITableView!
    
    @IBAction func addCategory(sender: AnyObject) {
        let alert = UIAlertController(title: "New Category",
            message: "Add a new category. Please choose the name below.",
            preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save",
            style: .Default,
            handler: { (action:UIAlertAction) -> Void in
                let cat: String? = alert.textFields!.first?.text
                
                self.refreshButton.enabled = false
                DataManager.CategoryHandler.addCategory(cat, completionHandler: self.reloadFromCoreData)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
            style: .Default) { (action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (textField: UITextField) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert,
            animated: true,
            completion: nil)
    }
    
    @IBAction func reloadData(sender: AnyObject) {
        refreshButton.enabled = false
        addButton.enabled = false
        
        //enable cell interaction
        for cell in tableView.visibleCells {
            cell.userInteractionEnabled = false
            cell.textLabel!.textColor = UIColor.grayColor()
        }
        
        DataManager.updateCoreDataFromServer(reloadFromCoreData)
    }
    
    func reloadFromCoreData() {
        categories = DataManager.CategoryHandler.getCategories()
        
        tableOutlet.reloadData()
        //disable cell interaction
        for cell in tableView.visibleCells {
            cell.userInteractionEnabled = true
            cell.textLabel!.textColor = UIColor.blackColor()
        }
        refreshButton.enabled = true
        addButton.enabled = true
        CATransaction.flush()
    }
    
    override func viewWillAppear(animated: Bool) {
        reloadFromCoreData()
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        cell.textLabel?.text = categories[indexPath.row].name

        return cell
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        passToRecipesView = DataManager.RecipeHandler.getRecipes(categories[indexPath.row].objectId!)
        
        titleToPass = categories[indexPath.row].name!
        
        globalCurrentCategoryId = categories[indexPath.row].objectId!
        
        return indexPath
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            let alert = UIAlertController(title: "Delete Category",
                message: "Are you sure you want to delete this category and all recipes belonging to it?",
                preferredStyle: .Alert)
            
            let deleteAction = UIAlertAction(title: "Delete",
                style: .Default,
                handler: { (action:UIAlertAction) -> Void in
                    let deleteThis = self.categories[indexPath.row].objectId! as NSNumber
                    DataManager.CategoryHandler.deleteCategory(deleteThis, completionHandler: self.reloadFromCoreData)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel",
                style: .Default) { (action: UIAlertAction) -> Void in
            }
            
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            
            presentViewController(alert,
                animated: true,
                completion: nil)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showRecipes" {
            let svc = segue.destinationViewController as! RecipesViewController;
            
            svc.recipes = passToRecipesView
            
            svc.titleBar.title = titleToPass
            
        }
    }
    

}
