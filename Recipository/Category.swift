//
//  Category.swift
//  Recipository
//
//  Created by Jakob Hviid on 21/11/15.
//  Copyright © 2015 hviidNet. All rights reserved.
//

import Foundation
import CoreData

class Category : NSManagedObject{
    var Id: Int = 0
    var Name: String = ""
    var Recipes: [Recipe] = [Recipe]()
    
    init (id: Int, name:String, recepies:[Recipe]) {
        self.Id = id
        self.Name = name
        self.Recipes = recepies
    }
    
    class CoreDataKeys {
        static let EntityName = "Category"
        static let Id = "id"
        static let Name = "name"
    }
    
    class JsonKeys {
        static let Array = "Categories"
        static let Id = "Id"
        static let Name = "Name"
    }
}